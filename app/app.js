(function() {
  'use strict';

angular.module('App', ['ui.router', 'MomentModule', 'City'])
    .constant('WEATHER_CONFIG', {
        apiKey:'073dae78d0525776a5462d4d9a0ad7bd'
    })

    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('forecast', {
            url: '/forecast/:cityname',
            views: {
                'main@': {
                    templateUrl: './city/forecast/forecast.view.html',
                    controller: 'ForecastController',
                    controllerAs: 'ctrl'
                }
            },
            params: {
                cityname:''
            }
        });
    }]);
    // .config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
    //     uiGmapGoogleMapApiProvider.configure({
    //         key: 'AIzaSyBEE00CA-3IfAn_YphwEaQoHihUEanvwRk',
    //         v: '3.20',
    //         libraries: 'weather, geometry, visualization'
    //     })
    // }]);
}());
(function () {
    'use strict';
    angular
        .module('MomentModule', [])
        .factory('Moment', function ($window) {
            return $window.moment;
        });
}());

(function () {
    'use strict';
    angular.module('City').component('cityList', {
        templateUrl: './city/city-list.view.html',
        controller:['$state', function ($state) {
            var ctrl = this;
            ctrl.cityNames = ['Leeuwarden', 'Skopje', 'Riga', 'Cologne', 'Gibraltar'];

            ctrl.showForecast = function (cityName) {
                ctrl.activeCity = cityName;
                $state.go('forecast', {cityname:cityName},{reload:true});
            };
        }],
        controllerAs:'ctrl'
    })
}());
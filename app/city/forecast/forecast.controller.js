(function () {
   'use strict';

   angular.module('City').controller('ForecastController', ['$stateParams', 'Moment', 'CityService', 'ForecastRepository', function ($stateParams, Moment, CityService, ForecastRepository) {
       var ctrl = this;

       ctrl.dayAmnt = 5;
       ctrl.days = [];
       ctrl.cityName = $stateParams.cityname;

       ctrl.getDay = function (plusAmnt) {
           var daysInFuture = plusAmnt + 1;
           return moment().add(daysInFuture,'day').format('dddd');
       };

       ctrl.generateForecastKeys = function () {
           var result = [];
            for(var i = 0; i < ctrl.dayAmnt; i++) {
                result[i] = {
                    tileName: ctrl.getDay(i),
                    forecastKey: ctrl.cityName + '#' + i,
                    cityName: ctrl.cityName
                }
            }
            return result;
       };

       CityService.GetForecast(ctrl.cityName).then(function (result) {
           ForecastRepository.SetForecastData(ctrl.cityName, result.data.list,ctrl.dayAmnt);
           ctrl.days = ctrl.generateForecastKeys();
       });
   }]);
}());
(function () {
    'use strict';


    angular.module('City').service('ForecastRepository', function () {
        var self = this;

        self.ForecastData = {};

        self.SetForecastData = function (cityName, data, days) {
            for (var i = 0; i< days; i++) {
                self.ForecastData[cityName + '#' + i] = data[i];
            }
        };

        self.GetForecast = function (key) {
            return self.ForecastData[key];
        };

        return self
    });


}());
(function () {
    'use strict';

        angular.module('City').directive('weatherTile', function ()  {
            return {
                restrict:'A',
                replace:true,
                templateUrl:'./city/weather-tile.view.html',
                scope:{
                    cityname: '=',
                    tilename: '=',
                    forecast: '='
                },
                controller: ['$scope', '$state', 'CityService', 'ForecastRepository', function($scope, $state, CityService, ForecastRepository) {
                    var ctrl = this;

                    if ($scope.forecast) {
                        ctrl.cityData = ForecastRepository.GetForecast($scope.forecast);
                    } else if ($scope.cityname) {
                        CityService.GetCityData($scope.cityname).then(function (result) {
                            ctrl.cityData = result.data;
                        });
                    }
                }],
                controllerAs:'ctrl'
            }
        })
}());
(function () {
    'use strict';

    angular.module('City', []).service('CityService', ['$http', 'WEATHER_CONFIG', function ($http, WEATHER_CONFIG) {
        var self = this;

        self.GetCityData = function (cityName) {
            return $http({method:'GET', url: 'http://api.openweathermap.org/data/2.5/weather?q='+ cityName +'&appid=' + WEATHER_CONFIG.apiKey});
        };

        self.GetForecast = function (cityName) {
            return $http({method:'GET', url: 'http://api.openweathermap.org/data/2.5/forecast?q='+ cityName +'&appid=' + WEATHER_CONFIG.apiKey});
        };

        return self
    }]);
}());
# `Weather app Exercise` — An exercise in Javascript UX

This is an example app demonstrating several aspects of a front-end client. It retrieves weather data
for 5 european cities, and optionally displays a five-day forecast for each city.

!!!
Opening up index.html on a server is enough to make it work. Opening it on the local filesystem will create CORS errors,
due to your browsers strict Cross-Origin settings. Open the source folder in an IDE and/or run index.html from a server.

## Dependencies:

This project was made in [AngularJS][angularjs], using [ui.router] for 'navigation' and [Moment] for correct day display,
Font awesome is used for icons.

Javascript:
AngularJS, UI-Router, MomentJS

CSS:
Font-Awesome

### What I did.

I started out with a seed project for AngularJS and stripped out all the boilerplate.
After that I created a flat html/css view, a wireframe of how I wanted the application to work.
I extracted components and directives from this view, added controllers where needed and hooked them up to
a CityService, which is responsible for fetching the data from the API.

-weatherTile directive
This is a reusable tile displaying information on temperature, humidity, air-pressure and the given forecast for a day.
It is written as a directive with a controller. This makes it both responsible for retrieving it's own data
and handling some view-logic. Normally I would advice to split up this responsibility into two separate entities,
but for now I felt like it would be more rewarding to invest extra time in integrating a map (*spoiler* Not a good move. 4 hours later and no map.)

-ForecastRepository service
I wanted to use the same component for both forecasting and the current weather of a city, which is why I
introduced a Forecast Repository service which is responsible for holding on to the forecast data. This data is retrieved
by the individual directives in view.

-uiRouter
The main reason for including ui-router over the default ngRoute is one of personal comfort. It doesn't add anything
extra in the limited scope of this exercise, but I'm used to it.

-MomentJS
Forecast titles should at least make sense. So I introduced momentjs to help me with the days of the week.
A wrapper in shared makes it available as an angular module, for good form.

###Conventions
- Single quotes in .js (out of habit)
- No service calling another service.
- Restricted use of $scope in controller.
- Every file encapsulated in module pattern and in strict mode.
- Double checked for no accidental ES2015 syntax usages. (Which I assume is not allowed, for the same reasons as TypeScript)
- Capitalized public functions, pascalCased privates.

- The only piece of comment I simply could not remove is located in App.js, for future implementation.
- Did add Lodash as a Util dependency at some point, but finally decided to remove it and replace the few loops that were included with simple for-loops, for simplicity.
- Did have all dependencies locally in an assets-map, but replaced the existing links with CDN-references to keep the file size small.
- Did integrate Bootstrap at some point, but decided to remove it in favor of 20 'lines' of css.